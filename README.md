# Jekyll-Introspect

An ultra-minimal landing page template with a big cover photo.


## Installation

Add this line to your Jekyll site's Gemfile:

```ruby
gem "jekyll-introspect"
```

And add this line to your Jekyll site:

```yaml
theme: jekyll-introspect
gems:
  - jekyll-bower
  - liquid-google-analytics
bower:
  introspect: "https://templated.co/introspect/download"

```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install jekyll-introspect

## Usage

A simple website template having beautiful landing page, blog posts and services/products listing.

To add blog to your website, create `_posts` folder under root directory. Write front matter inside post as follows: 
```yaml
	---
	title: Morbi interdum mol
	image_path: images/pic02.jpg
	layout: page
	---
```

To display services, create `_services` folder inside root directory.
Create service as `service-name.md` in `_services` folder.
Add front matter in each service as:
```yaml
	---
	title: Aliquam Nulla
	icon: fa-laptop
	short_description: Ut convallis, sem sit amet interdum consectetuer, odio augue aliquam leo, nec dapibus tortor nibh sed.
	categories: services
	layout: page
	---

```

Create collection in `_config.yml` file as 
```ruby
	collections:
		services:
    	output: true
```

Update `social_links` in `_config.yml` file according to your social account url.


## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/hello. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## Development

To set up your environment to develop this theme, run `bundle install`.

You theme is setup just like a normal Jelyll site! To test your theme, run `bundle exec jekyll serve` and open your browser at `http://localhost:4000`. This starts a Jekyll server using your theme. Add pages, documents, data, etc. like normal to test your theme's contents. As you make modifications to your theme and to your content, your site will regenerate and you should see the changes in the browser after a refresh, just like normal.

When your theme is released, only the files in `_layouts`, `_includes`, and `_sass` tracked with Git will be released.

## License

The theme is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

